# ANSIBLE PLAYBOOKS

## roles
- 00-remoteuser - create remote `ansible` user if missing
- 01-defaults - install basic tools, customization and basic security (nft, sshd)
- 02-maintenance - update installed packages
- apache - mods, virtualhost, /etc/hosts
- mariadb - backup user, create database&user with permissions
- php - mods, variables
- user - user with groups

# playbooks
- host-eir_bullseye.yml - LAMP, users: charles, steiner, alembiq, cert, prazskahlidka, snempohanskychobci, zip
- host-octopi.yml
- host-devbox.yml
- host-rpi_brno.yml
- host-verdandi.yml


```

## TODO

- redo user-* to sets of variables that would be included and/or modified in host-* playbooks and used by user role
- user role should be importing ssh keys
- 01-defaults: configure etckeeper, tzdata
- 01-defatuls: configure locales
- bind9 & domain zones
- ?tailscale/headscale vs openvpn?
- /etc/hosts ipv6
- backup mechanics
- set cron (tt-rss)
- config mysql


## [modules](https://docs.ansible.com/ansible/2.8/modules/list_of_all_modules.html) to use
 - https://docs.ansible.com/ansible/2.8/modules/shell_module.html#shell-module
 - https://docs.ansible.com/ansible/2.8/modules/script_module.html#script-module
 - https://docs.ansible.com/ansible/2.8/modules/timezone_module.html#timezone-module
 - https://docs.ansible.com/ansible/2.8/modules/systemd_module.html#systemd-module
 - https://docs.ansible.com/ansible/2.8/modules/service_module.html#service-module
 - https://docs.ansible.com/ansible/2.8/modules/hostname_module.html#hostname-module
 - https://docs.ansible.com/ansible/2.8/modules/cron_module.html#cron-module
 - https://docs.ansible.com/ansible/2.8/modules/copy_module.html#copy-module
 - https://docs.ansible.com/ansible/2.8/modules/ini_file_module.html#ini-file-module
 - https://docs.ansible.com/ansible/2.8/modules/replace_module.html#replace-module
 - https://docs.ansible.com/ansible/2.8/modules/template_module.html#template-module
 - https://docs.ansible.com/ansible/2.8/modules/git_module.html#git-module
 - https://docs.ansible.com/ansible/2.8/modules/mysql_variables_module.html#mysql-variables-module
