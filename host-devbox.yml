---
- hosts: devbox.svornosti # https://gitlab.alembiq.net/etc/etc-devbox/-/issues?scope=all&state=all
  name: configure devbox - apache, samba
  # gather_facts: false
  vars_files:
    - .host-devbox.yml
  vars:
    remote_ansible_user: 'ansible'
    remote_ansible_user_uid: 150
    root_ssh_user: 'charles'
    root_ssh_key: '~/.ssh/charles@verdandi-2021-07-09'
    bash_configs:
      - '.bashrc'
      - '.bash_profile'
      - '.bash_aliases'
      - '.bash_logout'
    user_name: "ernedar"
    user_groups: "sambashare"
    mariadb_backup_user: 'readonlyuser'
    mariadb_backup_folder: '/home/{{ user_name }}/backup/mysql'
    php_version: "8.1"
    php_mod:
      - fpm
      - mysql
      - xml
      - gd
      - zip
      - curl
      - cli
      - mbstring
    apache_mod:
      - proxy_fcgi
      - expires
      - headers
      - rewrite
      - ssl
      - http2
      - brotli
      - mpm_itk
    apache_conf:
      - php{{ php_version }}-fpm
      - adminer
    sites:
        - { domain: 'bistro', database: 'bistro', ipv4: '0.0.0.0', password: '{{ bistro_password }}', aliases: []  }
        - { domain: 'banka', database: 'banka', ipv4: '0.0.0.0', password: '{{ bank_password }}', aliases: []  }
        - { domain: 'devbox', database: 'devbox', ipv4: '0.0.0.0', password: '', aliases: ['devel']  }
    samba_group: 'sambashare'
    samba_users:
      - { name: 'charles', group: '{{ samba_group }}', smbpasswd: '{{ charles_passwd }}', uid: 1000 }
      - { name: 'ernedar', group: '{{ samba_group }},adm,cdrom,sudo,plugdev,users,input,render,netdev,video', smbpasswd: '{{ ernedar_passwd }}', uid: 1013 }
    symlinks:
      - { source: '/var/log/apache2/error.log', target: '/home/{{ user_name }}/logs/error.log'}
      - { source: '/var/log/apache2/access.log', target: '/home/{{ user_name }}/logs/access.log'}
      - { source: '/var/log/apache2/banka_error.log', target: '/home/{{ user_name }}/logs/banka_error.log'}
      - { source: '/var/log/apache2/bistro_error.log', target: '/home/{{ user_name }}/logs/bistro_error.log'}
      - { source: '/var/log/apache2/devbox_error.log', target: '/home/{{ user_name }}/logs/devbox_error.log'}
      - { source: '/var/log/php{{ php_version }}-fpm.log', target: '/home/{{ user_name }}/logs/php.log'}
      - { source: '/etc/apache2/sites-enabled/bistro.conf', target: '/home/{{ user_name }}/config/bistro.conf'}
      - { source: '/etc/apache2/sites-enabled/banka.conf', target: '/home/{{ user_name }}/config/banka.conf'}
      - { source: '/etc/apache2/sites-enabled/devbox.conf', target: '/home/{{ user_name }}/config/devbox.conf'}
      - { source: '/srv/devbox/htdocs', target: '/home/{{ user_name }}/htdocs'}
      - { source: '/srv/bistro/htdocs', target: '/home/{{ user_name }}/bistro'}
      - { source: '/srv/banka/htdocs', target: '/home/{{ user_name }}/banka'}
      - { source: '/srv/backup', target: '/home/{{ user_name }}/backup'}
      - { source: '/etc/hosts', target: '/home/{{ user_name }}/config/hosts.conf'}
      - { source: '/home/ernedar/.ssh/config', target: '/home/{{ user_name }}/config/ssh.conf'}
      - { source: '/etc/php/{{ php_version }}/fpm/php.ini', target: '/home/{{ user_name }}/config/php.ini'}
      - { source: '/etc/php/{{ php_version }}/fpm/php-fpm.conf', target: '/home/{{ user_name }}/config/php-fpm.conf'}
      - { source: '/srv/bistro/htdocs/log/', target: '/home/{{ user_name }}/logs/bistro'}


      # - { source: '/var/lib/mysql', target: '/home/{{ user_name }}/database'}

  roles:
    - 00-remoteuser
    - 01-defaults
    - 02-maintenance

  tasks:

    - name: Change repo from stable to testing
      ansible.builtin.replace:
        path: /etc/apt/sources.list
        regexp: '{{ item.search }}'
        replace: '{{ item.replace }}'
      with_items:
        - { search: 'bullseye', replace: 'testing' }

    - name: set hostname
      raw: "hostnamectl set-hostname devbox"

    - name: configure hosts
      lineinfile:
        backup: yes
        state: present
        dest: /etc/hosts
        regexp: '^{{ item.search }}'
        line: '{{ item.replace }}'
      with_items:
        - { search: '127.0.1.1               raspberrypi', replace: '127.0.1.1 devbox raspberrypi adminer bistro banka' }

    - name: Ensure group `{{ samba_group }}` exists with correct gid
      ansible.builtin.group:
        name: "{{ samba_group }}"
        state: present
        gid: 900

    - name: install packages
      apt:
        update_cache: yes
        cache_valid_time: 3600
        install_recommends: yes
        pkg:
          - cifs-utils
          - samba
          - python3
          - python3-pip
          - git
          - pandoc
          - adminer
          - localepurge
          - mc
          - composer

    - name: samba_users | adding samba users to samba groups
      user:
        name: "{{ item['name'] }}"
        groups: "{{ item['group'] }}"
        uid: "{{ item['uid'] }}"
        append: yes
      become: true
      with_items: "{{ samba_users }}"

    - name: samba_users | creating samba user passwords # noqa 301 306
      shell: "(echo {{ item['smbpasswd'] }}; echo {{ item['smbpasswd'] }}) | smbpasswd -s -a {{ item['name'] }}"
      become: true
      with_items: "{{ samba_users }}"

    - name: samba configuration
      ansible.builtin.template:
        src: templates/devbox-samba.conf
        dest: /etc/samba/smb.conf
        owner: root
        group: root
        mode: '0644'
        backup: yes

    - name: disable BT and WIFI
      blockinfile:
        dest:  /boot/config.txt
        create: yes
        block: |
          [all]
          #dtoverlay=disable-wifi
          dtoverlay=disable-bt
        # backup: yes

    - name: disable BT service.
      service:
        name: "{{item}}"
        state: stopped
        enabled: false
      with_items:
        - bluetooth


    - name: configure swapfile
      lineinfile:
        backup: yes
        state: present
        dest: /etc/dphys-swapfile
        regexp: '^{{ item.search }}'
        line: '{{ item.replace }}'
      with_items:
        - { search: 'CONF_SWAPSIZE', replace: 'CONF_SWAPSIZE=4096' }
        - { search: 'CONF_MAXSWAP', replace: 'CONF_MAXSWAP=4096' }

    - name: restart swap
      raw: "dphys-swapfile swapoff && dphys-swapfile setup && dphys-swapfile swapon"

    - name: setting up cron
      blockinfile:
        dest: /var/spool/cron/crontabs/ernedar
        block: |
          * * * * * echo "<h1>DEVBOX CPU" >/srv/devbox/htdocs/.HEADER.html; vcgencmd measure_temp >>/srv/devbox/htdocs/.HEADER.html; echo "</h1><h2>"; ip -4 addr | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep -v 127 >>/srv/devbox/htdocs/.HEADER.html; echo "</h2><hr>" >>/srv/devbox/htdocs/.HEADER.html >/dev/null 2>&1
          2 * * * * pandoc -s -f  markdown ~/.motd --template .template.html -o /srv/devbox/htdocs/.README.html >/dev/null 2>&1
        backup: yes
        create: yes
        owner: ernedar
        group: crontab
        mode: 0600

    - name: configure locale
      lineinfile:
        backup: yes
        state: present
        dest: /etc/locale.gen
        regexp: '^{{ item.search }}'
        line: '{{ item.replace }}'
      with_items:
        - { search: '# en_US.UTF-8', replace: 'en_US.UTF-8 UTF-8' }
        - { search: '# cs_CZ.UTF-8', replace: 'cs_CZ.UTF-8 UTF-8' }

    - name: generate locale
      raw: "locale-gen"

    - name: Create a directories for configs
      ansible.builtin.file:
        path: "{{ item }}"
        state: directory
        mode: '0755'
      with_items:
        - /home/{{ user_name }}/logs
        - /home/{{ user_name }}/config

    - name: get bistro
      git:
        repo: https://gitlab.com/alembiq/bistro.git
        dest: /srv/bistro/htdocs
        clone: yes
        update: yes

    - name: get banka
      git:
        repo: https://gitlab.alembiq.net/larp/di-banka-2004.git
        dest: /srv/banka/htdocs
        clone: yes
        update: yes

    - name: change ownership of the /srv directory
      ansible.builtin.file:
        path: "{{ item }}"
        state: directory
        recurse: yes
        owner: '{{ user_name }}'
        group: '{{ user_name }}'
      with_items:
        - /srv/devbox
        - /srv/bistro
        - /srv/banka

    - name: bistro composer
      raw: "su ernedar -c 'cd /srv/bistro/htdocs && composer install --ignore-platform-reqs'"

    - name: opening firewall for raspotify, octoprint, saned, cups
      blockinfile:
        dest: /etc/nftables.conf
        insertafter: '^                tcp dport 22 ct state new limit rate 15/minute accept'
        block: |
                          tcp dport { 80, 443, } accept
                          udp dport netbios-ns accept comment "Accept NetBIOS Name Service (nmbd)"
                          udp dport netbios-dgm accept comment "Accept NetBIOS Datagram Service (nmbd)"
                          tcp dport netbios-ssn accept comment "Accept NetBIOS Session Service (smbd)"
                          tcp dport microsoft-ds accept comment "Accept Microsoft Directory Service (smbd)"
        backup: yes

    - name: PHP
      include_role:
        name: php

    - name: MariaDB
      include_role:
        name: mariadb

    - name: automysqlbackup configuring
      lineinfile:
        dest=/etc/default/automysqlbackup
        regexp='^#PASSWORD='
        line="PASSWORD={{ mariadb_password }} "
        state=present
        backup=yes

    - name: apache
      include_role:
        name: apache

    - name: configure apache info/status
      lineinfile:
        backup: yes
        state: present
        dest: "{{ item }}"
        regexp: 'Require local'
        line: '#Require local'
      with_items:
        - /etc/apache2/mods-enabled/info.conf
        - /etc/apache2/mods-enabled/status.conf

    - name: configure autoindex
      lineinfile:
        backup: yes
        state: present
        dest: /etc/apache2/mods-available/autoindex.conf
        regexp: '^{{ item.search }}'
        line: '{{ item.replace }}'
      with_items:
        - { search: 'ReadmeName README.html', replace: 'ReadmeName .README.html' }
        - { search: 'HeaderName HEADER.html', replace: 'HeaderName .HEADER.html' }

    - name: Ensure services are running and set to start on boot.
      service:
        name: "{{item}}"
        state: restarted
        enabled: true
      with_items:
        - smbd
        - nftables
        - apache2

    - name: cleanup
      raw: "rm -rf /home/ernedar/devbox /home/ernedar/bistro /home/ernedar/banka /srv/devbox/htdocs/index.html && a2enconf adminer && a2enmod info status"

    - name: Create a symbolic links
      ansible.builtin.file:
        src: '{{ item.source }}'
        dest: "{{ item.target }}"
        state: link
      with_items:
        - "{{ symlinks }}"

# TODO etckeeper
# TODO tailscale
